import CartParser from "./CartParser";
import path from "path";

let parser;
const uuidTemplate =
  /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i;

beforeEach(() => {
  parser = new CartParser();
});

describe("CartParser - unit tests", () => {
  describe("validate", () => {
    test("should return array of errors if column header is invalid", () => {
      const invalidHeadersLine = "Product name,Cost,Quantity";

      const result = parser.validate(invalidHeadersLine);

      expect(result.length).toBe(1);
      expect(result[0]).toEqual({
        type: parser.ErrorType.HEADER,
        row: 0,
        column: 1,
        message: `Expected header to be named "Price" but received Cost.`,
      });
    });

    test("should return array of errors if rows length is invalid", () => {
      const invalidBodyLine = "Product name,Price,Quantity\n9.00,2";

      const result = parser.validate(invalidBodyLine);

      expect(result.length).toBe(1);
      expect(result[0]).toEqual({
        type: parser.ErrorType.ROW,
        row: 1,
        column: -1,
        message: `Expected row to have 3 cells but received 2.`,
      });
    });

    test("should return array of errors if string cell is an empty string", () => {
      const invalidBodyLine = "Product name,Price,Quantity\n,9.00,2";

      const result = parser.validate(invalidBodyLine);

      expect(result.length).toBe(1);
      expect(result[0]).toEqual({
        type: parser.ErrorType.CELL,
        row: 1,
        column: 0,
        message: `Expected cell to be a nonempty string but received "".`,
      });
    });

    test("should return array of errors if value of positive number cell reduced to number is NaN", () => {
      const invalidBodyLine =
        "Product name,Price,Quantity\nMollis consequat,asda,2";

      const result = parser.validate(invalidBodyLine);

      expect(result.length).toBe(1);
      expect(result[0]).toEqual({
        type: parser.ErrorType.CELL,
        row: 1,
        column: 1,
        message: `Expected cell to be a positive number but received "asda".`,
      });
    });

    test("should return array of errors if value of positive number cell is not positive number", () => {
      const invalidBodyLine =
        "Product name,Price,Quantity\nMollis consequat,0.7,-32";

      const result = parser.validate(invalidBodyLine);

      expect(result.length).toBe(1);
      expect(result[0]).toEqual({
        type: parser.ErrorType.CELL,
        row: 1,
        column: 2,
        message: `Expected cell to be a positive number but received "-32".`,
      });
    });

    test("should return array of errors if csv line have errors of different type", () => {
      const invalidBodyLine =
        "Name,Pric,Quantity\nMollis consequat,0.7,-32\n,0.7,4\napple,aaaa,11";

      const result = parser.validate(invalidBodyLine);

      expect(result.length).toBe(5);
      result.forEach((err) =>
        expect(
          err.type === parser.ErrorType.HEADER ||
            err.type === parser.ErrorType.ROW ||
            err.type === parser.ErrorType.CELL
        )
      );
    });

    test("should return empty array when all rows are valid", () => {
      const validLine =
        "Product name,Price,Quantity\nMollis consequat,0.7,32\nScelerisque lacinia,18.90,1";

      const result = parser.validate(validLine);

      expect(result.length).toBe(0);
    });

    test("should return empty array if file have only valid column header", () => {
      const invalidHeadersLine = "Product name,Price,Quantity";

      const result = parser.validate(invalidHeadersLine);

      expect(result.length).toBe(0);
    });
  });

  describe("parseLine", () => {
    test("should return valid object", () => {
      const line = "Mollis consequat,0.7,32";

      const result = parser.parseLine(line);

      expect(Object.keys(result).length).toBe(4);
      expect(result.id).toMatch(uuidTemplate);
      expect(result.name).toBe("Mollis consequat");
      expect(result.price).toBe(0.7);
      expect(result.quantity).toBe(32);
    });
  });

  describe("calcTotal", () => {
    test("should return correct total price", () => {
      const cardItems = [
        {
          id: "3e6def17-5e87-4f27-b6b8-ae78948523a9",
          name: "Mollis consequat",
          price: 9,
          quantity: 2,
        },
        {
          id: "90cd22aa-8bcf-4510-a18d-ec14656d1f6a",
          name: "Tvoluptatem",
          price: 10.32,
          quantity: 1,
        },
        {
          id: "33c14844-8cae-4acd-91ed-6209a6c0bc31",
          name: "Scelerisque lacinia",
          price: 18.9,
          quantity: 1,
        },
        {
          id: "f089a251-a563-46ef-b27b-5c9f6dd0afd3",
          name: "Consectetur adipiscing",
          price: 28.72,
          quantity: 10,
        },
        {
          id: "0d1cbe5e-3de6-4f6a-9c53-bab32c168fbf",
          name: "Condimentum aliquet",
          price: 13.9,
          quantity: 1,
        },
      ];

      const result = parser.calcTotal(cardItems);

      expect(result).toBeCloseTo(348.32);
    });

    test("should return 0 if cardItems array is empty", () => {
      const cardItems = [];

      const result = parser.calcTotal(cardItems);

      expect(result).toBe(0);
    });
  });
});

describe("CartParser - integration test", () => {
  describe("parse", () => {
    test("should return correct JSON object for valid csv file", () => {
      const pathToTestData = path.resolve(__dirname, "testData.csv");

      const result = parser.parse(pathToTestData);

      expect(Object.keys(result).length).toBe(2);
      expect(result.items.length === 2);
      const firstItem = result.items[0];
      expect(Object.keys(firstItem).length).toBe(4);
      expect(firstItem.id).toMatch(uuidTemplate);
      expect(firstItem.name).toBe("Mollis consequat");
      expect(firstItem.price).toBe(9.0);
      expect(firstItem.quantity).toBe(2);
      const secondItem = result.items[1];
      expect(Object.keys(secondItem).length).toBe(4);
      expect(secondItem.id).toMatch(uuidTemplate);
      expect(secondItem.name).toBe("Tvoluptatem");
      expect(secondItem.price).toBe(10.32);
      expect(secondItem.quantity).toBe(1);
      expect(result.total).toBeCloseTo(28.32);
    });
  });
});
